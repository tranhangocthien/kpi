﻿using System;

namespace KPI
{
    [Serializable]
    public class Kpi : ICloneable
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public double Kpi1 { get; set; }//số người được gán tên trên tổng số người
        public double Kpi2 { get; set; }//số task được cập nhật tiến độ trên tổng số task
        public double Kpi3 { get; set; }//Số task cập nhật theo chỉ đạo / tổng số task được chỉ đạo cập nhật (hashtag #BGĐ)
        public double Kpi4 { get; set; }//Số task không thay đổi due date (trừ tag #BGĐ) / tổng số task (Trừ tag #BGĐ)
        public double Kpi5 { get; set; }//Tỉ lệ số task hoàn thành / Tổng số task có due date trong khoảng
        public double Kpi6 { get; set; }//Số task đầy đủ thông tin / tổng số task
        public double Kpi7 { get; set; }//Tỉ lệ số task được cập nhật bởi chức danh PA trong những việc không gán tên PA / tổng số task được cập nhật
        public double Kpi8 { get; set; }//Số task được gán tên / tổng số task
        public double Kpi9 { get; set; }//Tỉ lệ số task được cập nhật bởi cán bộ công nhân viên
        public double Kpi10 { get; set; }//Trung bình khối lượng hoàn thành công việc
        public int TotalTask { get; set; }
        public int OwnTask { get; set; }
        public int TotalPeople { get; set; }
        public int SuccessTask { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}