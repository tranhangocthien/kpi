﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using VecoLib.Dao;
using VecoLib.Model;

namespace KPI
{
    public class Dao : AbstractDao<User>
    {
        public Dao(string connectionString) : base(connectionString)
        {
        }

        public Dao(string server, string port, string database, string userId, string password) : base(server, port,
            database, userId, password)
        {
        }

        public bool UpdateByAssistance(int issueId, DateTime? from, DateTime? to)
        {
            try
            {
                OpenConnection();

                var sql = @"SELECT
	                            a.*
                            FROM
	                            journals a
                            INNER JOIN issues b ON a.journalized_id = b.id
                            AND b.assigned_to_id NOT IN (
	                            SELECT
		                            x.id
	                            FROM
		                            users x
	                            INNER JOIN members y ON x.id = y.user_id
	                            INNER JOIN issues z ON z.project_id = y.project_id
	                            AND z.id = @issueId
	                            INNER JOIN member_roles k ON k.member_id = y.id
	                            AND k.role_id = 6
                            )
                            AND b.id = @issueId
                            AND a.journalized_type = 'Issue'
                            INNER JOIN projects c ON b.project_id = c.id
                            INNER JOIN members d ON c.id = d.project_id
                            INNER JOIN member_roles e ON e.member_id = d.id
                            AND e.role_id = 6
                            WHERE
	                            a.created_on >= @from
                            AND a.created_on <= @to
                            GROUP BY
	                            a.id";
                var cmd = new MySqlCommand(sql, Conn);
                cmd.Parameters.AddWithValue("@issueId", issueId);
                cmd.Parameters.AddWithValue("@from", from);
                cmd.Parameters.AddWithValue("@to", to);
                var reader = cmd.ExecuteReader();
                var result = reader.Read();
                reader.Close();

                CloseConnection();
                return result;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error", e);
                CloseConnection();
            }

            return false;
        }

        public override User CreateItem(MySqlDataReader reader)
        {
            throw new NotImplementedException();
        }

        public override User GetItem(int id)
        {
            throw new NotImplementedException();
        }

        public override List<User> GetItems()
        {
            throw new NotImplementedException();
        }

        public override List<User> GetItems(int userId)
        {
            throw new NotImplementedException();
        }

        public override string Insert(User obj)
        {
            throw new NotImplementedException();
        }

        public override string Update(User obj)
        {
            throw new NotImplementedException();
        }

        public override string Delete(User obj)
        {
            throw new NotImplementedException();
        }
    }
}