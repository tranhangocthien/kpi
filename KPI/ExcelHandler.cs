﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using VecoLib.Dao;
using VecoLib.Model;

namespace KPI
{
    public class ExcelHandler
    {
        public static ExcelHandler Handler;

        public static ExcelHandler Instance()
        {
            return Handler ?? (Handler = new ExcelHandler());
        }

        public void CopyFile(string sourcePath, string destinationPath, string sourceFileName)
        {
            try
            {
                var dess = Regex.Split(destinationPath, @"\\");
                var desPath = "";
                for (var i = 0; i < dess.Length - 1; i++)
                    desPath += dess[i] + "\\";
                var desFileName = dess[dess.Length - 1];
                var sourceFile = Path.Combine(sourcePath, sourceFileName);
                var destFile = Path.Combine(desPath, desFileName);

                if (!Directory.Exists(desPath))
                    Directory.CreateDirectory(desPath);

                File.Copy(sourceFile, destFile, true);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error", e);
            }
        }

        #region KPI

        public void Kpi(string inputFile, string saveFile)
        {
            try
            {
                CopyFile(ConfigurationManager.AppSettings["SourcePath"], saveFile,
                    ConfigurationManager.AppSettings["SourceFile"]);
                using (var desPck = new ExcelPackage(new FileInfo(saveFile)))
                {
                    var desSheet = desPck.Workbook.Worksheets[1];
                    var row = 2;
                    var assistances = new List<Employee>();
                    var pms = new List<Employee>();
                    var engineers = new List<Employee>();
                    Prepare(assistances, pms, engineers, inputFile, desSheet, ref row);

                    desSheet = desPck.Workbook.Worksheets[2];
                    row = 2;
                    foreach (var assistance in assistances)
                    {
                        var begin = row;
                        desSheet.Cells["A" + row].Value = assistance.User.Login;
                        desSheet.Cells["B" + row].Value = assistance.Institue;
                        desSheet.Cells["C" + row].Value = "PA";

                        foreach (var kpi in assistance.Kpis)
                        {
                            desSheet.Cells["D" + row].Value = kpi.ProjectName;
                            desSheet.Cells["E" + row].Value = kpi.TotalTask;
                            desSheet.Cells["F" + row].Value = Math.Round(kpi.Kpi1 * 100, 2) + "%";
                            desSheet.Cells["G" + row].Value = Math.Round(kpi.Kpi6 * 100, 2) + "%";
                            desSheet.Cells["H" + row].Value = Math.Round(kpi.Kpi2 * 100, 2) + "%";
                            desSheet.Cells["I" + row].Value = Math.Round(kpi.Kpi3 * 100, 2) + "%";
                            desSheet.Cells["J" + row++].Value = Math.Round(kpi.Kpi4 * 100, 2) + "%";
                        }
                        desSheet.Cells["A" + begin + ":A" + (row - 1)].Merge = true;
                        desSheet.Cells["B" + begin + ":B" + (row - 1)].Merge = true;
                        desSheet.Cells["C" + begin + ":C" + (row - 1)].Merge = true;
                        desSheet.Cells["A" + begin + ":J" + (row - 1)].Style.Border.Bottom.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":J" + (row - 1)].Style.Border.Left.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":J" + (row - 1)].Style.Border.Right.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":J" + (row - 1)].Style.Border.Top.Style =
                            ExcelBorderStyle.Thin;
                    }

                    desSheet = desPck.Workbook.Worksheets[3];
                    row = 2;
                    foreach (var pm in pms)
                    {
                        var begin = row;
                        desSheet.Cells["A" + row].Value = pm.User.Login;
                        desSheet.Cells["B" + row].Value = pm.Institue;
                        desSheet.Cells["C" + row].Value = "PM";

                        foreach (var kpi in pm.Kpis)
                        {
                            desSheet.Cells["D" + row].Value = kpi.ProjectName;
                            desSheet.Cells["E" + row].Value = kpi.TotalTask;
                            desSheet.Cells["F" + row].Value = kpi.OwnTask;
                            desSheet.Cells["G" + row].Value = Math.Round(kpi.Kpi1 * 100, 2) + "%";
                            desSheet.Cells["H" + row].Value = Math.Round(kpi.Kpi6 * 100, 2) + "%";
                            desSheet.Cells["I" + row].Value = Math.Round(kpi.Kpi2 * 100, 2) + "%";
                            desSheet.Cells["J" + row].Value = Math.Round(kpi.Kpi3 * 100, 2) + "%";
                            desSheet.Cells["K" + row].Value = Math.Round(kpi.Kpi4 * 100, 2) + "%";
                            desSheet.Cells["L" + row].Value = Math.Round(kpi.Kpi7 * 100, 2) + "%";
                            desSheet.Cells["M" + row++].Value = Math.Round(kpi.Kpi5 * 100, 2) + "%";
                        }
                        desSheet.Cells["A" + begin + ":A" + (row - 1)].Merge = true;
                        desSheet.Cells["B" + begin + ":B" + (row - 1)].Merge = true;
                        desSheet.Cells["C" + begin + ":C" + (row - 1)].Merge = true;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Bottom.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Left.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Right.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Top.Style =
                            ExcelBorderStyle.Thin;
                    }

                    desSheet = desPck.Workbook.Worksheets[4];
                    row = 2;
                    foreach (var en in engineers)
                    {
                        if (en.Kpis.Count == 0 || en.Kpis.All(k => k.OwnTask == 0)) continue;
                        var begin = row;
                        desSheet.Cells["A" + row].Value = en.User.Login;
                        desSheet.Cells["B" + row].Value = en.Institue;
                        desSheet.Cells["C" + row].Value = "Kỹ sư";

                        foreach (var kpi in en.Kpis)
                        {
                            if (kpi.OwnTask == 0) continue;
                            desSheet.Cells["D" + row].Value = kpi.ProjectName;
                            desSheet.Cells["E" + row].Value = kpi.OwnTask;
                            desSheet.Cells["F" + row].Value = Math.Round(kpi.Kpi8 * 100, 2) + "%";
                            desSheet.Cells["G" + row].Value = Math.Round(kpi.Kpi6 * 100, 2) + "%";
                            desSheet.Cells["H" + row].Value = Math.Round(kpi.Kpi2 * 100, 2) + "%";
                            desSheet.Cells["I" + row].Value = Math.Round(kpi.Kpi3 * 100, 2) + "%";
                            desSheet.Cells["J" + row].Value = Math.Round(kpi.Kpi4 * 100, 2) + "%";
                            desSheet.Cells["K" + row].Value = Math.Round(kpi.Kpi9 * 100, 2) + "%";
                            desSheet.Cells["L" + row].Value = Math.Round(kpi.Kpi5 * 100, 2) + "%";
                            desSheet.Cells["M" + row++].Value = Math.Round(kpi.Kpi10, 2) + "%";
                        }
                        desSheet.Cells["A" + begin + ":A" + (row - 1)].Merge = true;
                        desSheet.Cells["B" + begin + ":B" + (row - 1)].Merge = true;
                        desSheet.Cells["C" + begin + ":C" + (row - 1)].Merge = true;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Bottom.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Left.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Right.Style =
                            ExcelBorderStyle.Thin;
                        desSheet.Cells["A" + begin + ":M" + (row - 1)].Style.Border.Top.Style =
                            ExcelBorderStyle.Thin;
                    }


                    desPck.Save();
                }
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error", e);
                MessageBox.Show("File is being opened by some applications!", "Error");
            }
        }

        #endregion

        #region Parse date

        private DateTime? ParseDate(string date)
        {
            DateTime? dateTime;

            try
            {
                dateTime = DateTime.ParseExact(date, "d/M/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                try
                {
                    dateTime = DateTime.ParseExact(date, "dd/M/yyyy", CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    try
                    {
                        dateTime = DateTime.ParseExact(date, "d/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            dateTime = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        catch (Exception e)
                        {
                            Logger.Log.Error("Error", e);
                            dateTime = null;
                        }
                    }
                }
            }

            return dateTime;
        }

        #endregion

        #region Prepare

        private void Prepare(List<Employee> assistances, List<Employee> pms, List<Employee> engineers, string inputFile,
            ExcelWorksheet desSheet, ref int row)
        {
            var issueDao = new IssueDao(ConfigurationManager.AppSettings["Server"],
                ConfigurationManager.AppSettings["Port"],
                ConfigurationManager.AppSettings["Database"], ConfigurationManager.AppSettings["UserId"],
                ConfigurationManager.AppSettings["Password"]);
            var projectDao = new ProjectDao(ConfigurationManager.AppSettings["Server"],
                ConfigurationManager.AppSettings["Port"],
                ConfigurationManager.AppSettings["Database"], ConfigurationManager.AppSettings["UserId"],
                ConfigurationManager.AppSettings["Password"]);


            var from = DateTime.ParseExact(ConfigurationManager.AppSettings["from"], "dd/MM/yyyy",
                CultureInfo.InvariantCulture);
            var to = DateTime.ParseExact(ConfigurationManager.AppSettings["to"], "dd/MM/yyyy",
                CultureInfo.InvariantCulture);

            using (var pck = new ExcelPackage(new FileInfo(inputFile)))
            {
                var sheet = pck.Workbook.Worksheets[1];
                for (var i = 2; i <= sheet.Dimension.Rows; i++)
                    try
                    {
                        if (sheet.Cells["A" + i].Value == null || sheet.Cells["A" + i].Value.ToString() == "")
                            continue;

                        var projectId = int.Parse(sheet.Cells["A" + i].Value.ToString());
                        var project = projectDao.GetItem(projectId);
                        var pPms = pms != null ? projectDao.GetRoleUsers(projectId, User.Role.PM) : null;
                        var pPas = projectDao.GetRoleUsers(projectId, User.Role.PA);
                        var engs = engineers != null ? projectDao.GetRoleUsers(projectId, User.Role.Engineer) : null;
                        var projectName = project.Name;
                        var totalPeople = int.Parse(sheet.Cells["C" + i].Value.ToString());

                        var issues = issueDao.GetItems(projectId, true, true);
                        if (issues == null) continue;
                        foreach (var issue in issues)
                            issue.Tags = issueDao.GetTags(issue.Id);
                        var kpi1 = KpiCalculator.Kpi1(issues, totalPeople);
                        var kpi2 = KpiCalculator.Kpi2(issues, from, to);
                        var kpi3 = KpiCalculator.Kpi3(issues, from, to,
                            int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                        var kpi4 = KpiCalculator.Kpi4(issues, from, to,
                            int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                        var kpi5 = KpiCalculator.Kpi5(issues, from, to);
                        var kpi6 = KpiCalculator.Kpi6(issues, from, to);
                        var kpi7 = KpiCalculator.Kpi7(issues, from, to);
                        var kpi = new Kpi
                        {
                            ProjectId = projectId,
                            ProjectName = projectName,
                            Kpi1 = kpi1,
                            Kpi2 = kpi2,
                            Kpi3 = kpi3,
                            Kpi4 = kpi4,
                            Kpi5 = kpi5,
                            Kpi6 = kpi6,
                            Kpi7 = kpi7,
                            TotalPeople = totalPeople,
                            TotalTask = issues.Count
                        };

                        #region Pm filter

                        if (pms != null)
                            foreach (var pm in pPms)
                            {
                                var p = pms.FirstOrDefault(e => e.User.Login == pm.Login);
                                if (p == null)
                                {
                                    p = new Employee();
                                    p.User = pm;
                                    pms.Add(p);
                                }

                                var isses = issues.Where(iss => iss.AssignedTo == pm.Login).ToList();
                                var clone = (Kpi) kpi.Clone();
                                clone.OwnTask = isses.Count;
                                clone.SuccessTask =
                                    isses.Count(iss =>
                                        iss.StatusId == 11 && iss.DueDate <= DateTime.Now && iss.DueDate >= from &&
                                        iss.DueDate <= to);
                                clone.Kpi8 = (double) isses.Count / issues.Count;
                                clone.Kpi6 = KpiCalculator.Kpi6(isses, from, to);
                                clone.Kpi2 = KpiCalculator.Kpi2(isses, from, to);
                                clone.Kpi3 = KpiCalculator.Kpi3(isses, from, to,
                                    int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                                clone.Kpi4 = KpiCalculator.Kpi4(isses, from, to,
                                    int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                                clone.Kpi5 = KpiCalculator.Kpi5(isses, from, to);
                                clone.Kpi9 = (double) issues.Count(iss =>
                                                 iss.AssignedToId == p.User.Id && iss.Journals.Any(j =>
                                                     j.UserId == p.User.Id && j.CreateOn >= from &&
                                                     j.CreateOn <= to)) / clone.OwnTask;
                                if (isses.Count == 0) clone.Kpi10 = 100;
                                else
                                    clone.Kpi10 = (double) isses.Sum(iss => iss.DoneRatio) / isses.Count;

                                p.Kpis.Add(clone);
                            }

                        #endregion

                        #region Pa filter

                        if (assistances != null)
                            foreach (var pa in pPas)
                            {
                                var p = assistances.FirstOrDefault(e => e.User.Login == pa.Login);
                                if (p == null)
                                {
                                    p = new Employee();
                                    p.User = pa;
                                    assistances.Add(p);
                                }

                                if(pa.Login == "giangnh13")
                                    Console.Write("");

                                var isses = issues.Where(iss => iss.AssignedTo == pa.Login).ToList();
                                var clone = (Kpi) kpi.Clone();
                                clone.OwnTask = isses.Count;
                                clone.SuccessTask =
                                    isses.Count(iss => iss.StatusId == 11 && iss.DueDate <= DateTime.Now);

                                var tilNowDone = issues.Where(iss => iss.AssignedTo == pa.Login && iss.DueDate <= DateTime.Now).ToList();

                                clone.Kpi8 = (double) isses.Count / issues.Count;
                                clone.Kpi6 = KpiCalculator.Kpi6(isses, from, to);
                                clone.Kpi2 = KpiCalculator.Kpi2(isses, from, to);
                                clone.Kpi3 = KpiCalculator.Kpi3(isses, from, to,
                                    int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                                clone.Kpi4 = KpiCalculator.Kpi4(isses, from, to,
                                    int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                                clone.Kpi5 = KpiCalculator.Kpi5(isses, from, to);
                                clone.Kpi9 = (double) issues.Count(iss =>
                                                 iss.AssignedToId == p.User.Id && iss.Journals.Any(j =>
                                                     j.UserId == p.User.Id && j.CreateOn >= from &&
                                                     j.CreateOn <= to)) / clone.OwnTask;
                                if (isses.Count == 0) clone.Kpi10 = 100;
                                else
                                    clone.Kpi10 = (double)tilNowDone.Sum(iss => iss.DoneRatio) / (double)tilNowDone.Count;

                                p.Kpis.Add(clone);
                            }

                        #endregion

                        #region Engineer filter

                        if (engineers != null)
                            foreach (var en in engs)
                            {
                                var log = en.Id + "-" + kpi.ProjectName + ": ";
                                var p = engineers.FirstOrDefault(e => e.User.Login == en.Login);
                                if (p == null)
                                {
                                    p = new Employee();
                                    p.User = en;
                                    engineers.Add(p);
                                }
                                var isses = issues.Where(iss => iss.AssignedTo == en.Login).ToList();
                                var clone = (Kpi) kpi.Clone();
                                clone.OwnTask = isses.Count;
                                clone.Kpi8 = (double) isses.Count / issues.Count;
                                clone.Kpi6 = KpiCalculator.Kpi6(isses, from, to);
                                clone.Kpi2 = KpiCalculator.Kpi2(isses, from, to);
                                clone.Kpi3 = KpiCalculator.Kpi3(isses, from, to,
                                    int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                                clone.Kpi4 = KpiCalculator.Kpi4(isses, from, to,
                                    int.Parse(ConfigurationManager.AppSettings["#BGD"]));
                                clone.Kpi5 = KpiCalculator.Kpi5(isses, from, to);
                                clone.Kpi9 = (double) issues.Count(iss =>
                                                 iss.AssignedToId == p.User.Id && iss.Journals.Any(j =>
                                                     j.UserId == p.User.Id && j.CreateOn >= from &&
                                                     j.CreateOn <= to)) / clone.OwnTask;
                                if (isses.Count == 0) clone.Kpi10 = 0;
                                else
                                    clone.Kpi10 = (double) isses.Sum(iss => iss.DoneRatio) / isses.Count;

                                log = "Số task đầy đủ thông tin: " + Math.Round(clone.Kpi6 * clone.OwnTask, 2) +
                                      "\tSố task được cập nhật tiến độ: " +
                                      Math.Round(clone.Kpi2 * clone.OwnTask, 2) +
                                      "\tTỉ lệ số task được cập nhật bởi cán bộ công nhân viên: " +
                                      Math.Round(clone.Kpi9 * clone.OwnTask, 2) +
                                      "\tTỉ lệ số task hoàn thành trên tổng số task có due date trong tháng: " +
                                      issues.Count(iss => iss.StatusId == 11) + "/" +
                                      issues.Count(iss => iss.DueDate >= from && iss.DueDate <= to);

                                Logger.Log.Error(log);

                                p.Kpis.Add(clone);
                            }

                        #endregion

                        if (desSheet != null)
                        {
                            desSheet.Cells["A" + row].Value = projectName;
                            desSheet.Cells["B" + row].Value = issues.Count;
                            desSheet.Cells["C" + row].Value = Math.Round(kpi1 * 100, 2) + "%";
                            desSheet.Cells["D" + row].Value = Math.Round(kpi2 * 100, 2) + "%";
                            desSheet.Cells["E" + row].Value = Math.Round(kpi3 * 100, 2) + "%";
                            desSheet.Cells["F" + row].Value = Math.Round(kpi4 * 100, 2) + "%";
                            desSheet.Cells["G" + row++].Value = Math.Round(kpi5 * 100, 2) + "%";
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Log.Error("Error", e);
                    }

                if (desSheet != null)
                {
                    desSheet.Cells["A2:G" + (row - 1)].Style.Border.Bottom.Style =
                        ExcelBorderStyle.Thin;
                    desSheet.Cells["A2:G" + (row - 1)].Style.Border.Left.Style =
                        ExcelBorderStyle.Thin;
                    desSheet.Cells["A2:G" + (row - 1)].Style.Border.Right.Style =
                        ExcelBorderStyle.Thin;
                    desSheet.Cells["A2:G" + (row - 1)].Style.Border.Top.Style =
                        ExcelBorderStyle.Thin;
                }
            }
        }

        #endregion

        #region KPI

        public void PaKpi(string inputFile, string savePath)
        {
            var assistances = new List<Employee>();
            var pms = new List<Employee>();
            var row = 0;
            Prepare(assistances, pms, null, inputFile, null, ref row);

            Run(pms, savePath, "PA");
            Run(assistances, savePath, "PA");
        }

        public void DevKpi(string inputFile, string savePath)
        {
            var engs = new List<Employee>();
            var row = 0;
            Prepare(null, null, engs, inputFile, null, ref row);

            RunForDev(engs, savePath);
        }

        private void RunForDev(List<Employee> list, string savePath)
        {
            try
            {
                var issueDao = new IssueDao(ConfigurationManager.AppSettings["Server"],
                    ConfigurationManager.AppSettings["Port"],
                    ConfigurationManager.AppSettings["Database"], ConfigurationManager.AppSettings["UserId"],
                    ConfigurationManager.AppSettings["Password"]);
                var row = 0;
                foreach (var em in list)
                {
                    var savefile = savePath + "\\" + em.User.Login + ".xlsx";
                    CopyFile(ConfigurationManager.AppSettings["SourcePath"], savefile,
                        ConfigurationManager.AppSettings["DevTemplate"]);
                    using (var pck = new ExcelPackage(new FileInfo(savefile)))
                    {
                        double x, y = 0;
                        var cells = pck.Workbook.Worksheets[1].Cells;
                        cells["C2"].Value = "Từ " + ConfigurationManager.AppSettings["from"] + " tới " +
                                            ConfigurationManager.AppSettings["to"];

                        row = 3;
                        cells["C" + row++].Value = em.User.Login;
                        row++;
                        cells["C" + row++].Value = "Kỹ sư";

                        row = 12;
                        double doneRatio = 0;

                        foreach (var kpi in em.Kpis)
                        {
                            var issues = issueDao.GetItems(kpi.ProjectId)
                                .Where(i => i.AssignedToId == em.User.Id).ToList();
                            cells["B" + row].Value = kpi.ProjectName;
                            cells["C" + row].Value = kpi.OwnTask;
                            cells["D" + row].Value = Math.Round(kpi.Kpi8 * 100, 2);
                            cells["E" + row].Value = Math.Round(kpi.Kpi6 * 100, 2);
                            cells["F" + row].Value = Math.Round(kpi.Kpi2 * 100, 2);
                            cells["G" + row].Value = Math.Round(kpi.Kpi9 * 100, 2);
                            cells["H" + row].Value = Math.Round(kpi.Kpi5 * 100, 2);
                            var done = (double) issues.Sum(i => i.DoneRatio) / issues.Count;
                            if (issues.Count == 0) done = 100;
                            doneRatio += done;
                            cells["I" + row++].Value = Math.Round(done, 2);
                        }

                        cells["C7"].Value = Math.Round(doneRatio / em.Kpis.Count, 2) + "%";

                        pck.Save();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error", e);
            }
        }

        public void Run(List<Employee> list, string savePath, string position)
        {
            try
            {
                var issueDao = new IssueDao(ConfigurationManager.AppSettings["Server"],
                    ConfigurationManager.AppSettings["Port"],
                    ConfigurationManager.AppSettings["Database"], ConfigurationManager.AppSettings["UserId"],
                    ConfigurationManager.AppSettings["Password"]);
                var row = 0;
                foreach (var em in list)
                {
                    var savefile = savePath + "\\" + em.User.Login + ".xlsx";
                    CopyFile(ConfigurationManager.AppSettings["SourcePath"], savefile,
                        ConfigurationManager.AppSettings["PATemplate"]);
                    if (em.User.Login == "phuongvt5")
                        Console.Write("");
                    using (var pck = new ExcelPackage(new FileInfo(savefile)))
                    {
                        double x, y = 0;
                        var cells = pck.Workbook.Worksheets[1].Cells;
                        cells["C2"].Value = "Từ " + ConfigurationManager.AppSettings["from"] + " tới " +
                                            ConfigurationManager.AppSettings["to"];

                        row = 3;
                        cells["C" + row++].Value = em.User.Login;
                        row++;
                        cells["C" + row++].Value = position;

                        row = 13;

                        var total = em.Kpis.Count;
                        var kpi1 = em.Kpis.Sum(k => k.Kpi1) / total;
                        cells["F" + row].Value = Math.Round(kpi1 * 100, 2) + "%";
                        cells["G" + row++].Value =
                            Math.Round(kpi1 * double.Parse(ConfigurationManager.AppSettings["factor1"]), 2);
                        var kpi6 = em.Kpis.Sum(k => k.Kpi6) / total;
                        cells["F" + row].Value = Math.Round(kpi6 * 100, 2) + "%";
                        cells["G" + row++].Value =
                            Math.Round(kpi6 * double.Parse(ConfigurationManager.AppSettings["factor6"]), 2);
                        var kpi2 = em.Kpis.Sum(k => k.Kpi2) / total;
                        cells["F" + row].Value = Math.Round(kpi2 * 100, 2) + "%";
                        cells["G" + row++].Value =
                            Math.Round(kpi2 * double.Parse(ConfigurationManager.AppSettings["factor2"]), 2);
                        var kpi3 = em.Kpis.Sum(k => k.Kpi3) / total;
                        cells["F" + row].Value = Math.Round(kpi3 * 100, 2) + "%";
                        cells["G" + row++].Value =
                            Math.Round(kpi3 * double.Parse(ConfigurationManager.AppSettings["factor3"]), 2);
                        var kpi4 = em.Kpis.Sum(k => k.Kpi4) / total;
                        cells["F" + row].Value = Math.Round(kpi4 * 100, 2) + "%";
                        cells["G" + row].Value =
                            Math.Round(kpi4 * double.Parse(ConfigurationManager.AppSettings["factor4"]), 2);
                        x = kpi1 + kpi2 + kpi3 + kpi4 + kpi6;
                        x /= 5;
                        cells["F11"].Value = Math.Round(x * 100, 2) + "%";

                        row = 20;
                        cells.Worksheet.InsertRow(row, em.Kpis.Count);

                        foreach (var kpi in em.Kpis)
                        {

                            cells["A" + row].Value = row - 19;
                            cells["B" + row].Value = kpi.ProjectName;
                            cells["C" + row].Value = kpi.TotalPeople;
                            cells["D" + row].Value = kpi.OwnTask;
                            cells["E" + row].Value = kpi.SuccessTask;
                            cells["F" + row++].Value = Math.Round(kpi.Kpi10, 2);
                            if (kpi.Kpi10 > 0)
                                y += kpi.Kpi10;

                        }
                        if (y != 0)
                        {
                            y /= em.Kpis.Count;
                        }

                        cells["F18"].Value = Math.Round(y, 2) + "%";
                        var test = Math.Round((x + y) / 2, 2);
                        cells["C7"].Value = Math.Round((x*100+y)/2,2) + "%";
                        Console.WriteLine("");
                        cells["C" + row++].Value = em.Kpis.Sum(k => k.TotalPeople);
                        row += 2;

                        foreach (var kpi in em.Kpis)
                            try
                            {
                                var issues = issueDao.GetItems(kpi.ProjectId, true)
                                    .Where(i => i.DueDate != null &&
                                                ((DateTime) i.DueDate).Date == DateTime.Today.AddDays(3) &&
                                                i.AssignedToId == em.User.Id).ToList();
                                if (issues.Count == 0) continue;
                                cells["B" + row].Value = kpi.ProjectName;
                                cells["B" + row + ":B" + (row + issues.Count - 1)].Merge = true;
                                foreach (var issue in issues)
                                {
                                    cells["C" + row].Value = issue.Subject;
                                    cells["D" + row].Value = ((DateTime) issue.DueDate).ToString("dd-MM-yyyy");
                                    cells["E" + row].Value = issue.DoneRatio;
                                    cells["F" + row++].Value = issue.CustomValues.FirstOrDefault(c => c.id == 2)?.value;
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Log.Error("Error", e);
                            }

                        pck.Save();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error", e);
            }
        }

        #endregion
    }
}