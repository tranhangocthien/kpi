﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using VecoLib.Model;

namespace KPI
{
    internal class KpiCalculator
    {
        private static readonly Dao Dao = new Dao(ConfigurationManager.AppSettings["server"],
            ConfigurationManager.AppSettings["port"],
            ConfigurationManager.AppSettings["database"], ConfigurationManager.AppSettings["userid"],
            ConfigurationManager.AppSettings["password"]);

        public static int CountingIssue(int projectId)
        {
            var issues = Dao.GetItems(projectId);

            if (issues == null) return 0;
            return issues.Count;
        }

        //số người được gán tên trên tổng số người
        public static double Kpi1(List<Issue> issues, int totalPeople)
        {
            var names = issues.GroupBy(i => i.AssignedToId, (key, g) => new {Name = key});
            var count = (double) names.Count(n => n.Name != null);
            if (totalPeople == 0) return 1;

            return count / totalPeople;
        }

        //số task được cập nhật tiến độ trên tổng số task
        public static double Kpi2(List<Issue> issues, DateTime? from, DateTime? to)
        {
            if (from == null || to == null) return 0;
            var count = issues.Count(i =>
                i.Journals.Any(j => j.CreateOn >= (DateTime) from && j.CreateOn <= (DateTime) to && j.PropKey == "2"));
            if (issues.Count == 0) return 1;
            return (double) count / issues.Count;
        }

        //Số task cập nhật theo chỉ đạo / tổng số task được chỉ đạo cập nhật (hashtag #BGĐ)
        public static double Kpi3(List<Issue> issues, DateTime? from, DateTime? to, int tagId)
        {
            double totalCount = issues.Count(i => i.Tags.Any(t => t.Id == tagId));
            double count = issues.Count(i =>
                i.Tags.Any(t => t.Id == tagId) && i.Journals.Any(j => j.CreateOn >= from && j.CreateOn <= to));
            if (totalCount == 0) return 1;

            return count / totalCount;
        }

        //Số task không thay đổi due date (trừ tag #BGĐ) / tổng số task (Trừ tag #BGĐ)
        public static double Kpi4(List<Issue> issues, DateTime? from, DateTime? to, int tagId)
        {
            var list = issues.Where(i => i.Tags.All(t => t.Id != tagId)).ToList();
            var notChangeDueDate = list.Count(i =>
                i.Journals.All(j =>
                    j.CreateOn < (DateTime) from || j.CreateOn > (DateTime) to || j.PropKey != "due_date"));
            if (list.Count == 0) return 1;

            return (double) notChangeDueDate / list.Count;
        }

        //Tỉ lệ số task hoàn thành / Tổng số task có due date trong khoảng
        public static double Kpi5(List<Issue> issues, DateTime? from, DateTime? to)
        {
            var successCount = issues.Count(i =>
                i.StatusId == 11 && i.DueDate != null && (DateTime) i.DueDate >= (DateTime) from &&
                (DateTime) i.DueDate <= (DateTime) to);
            var totalCount = issues.Count(i => i.DueDate != null &&
                (DateTime) i.DueDate >= (DateTime) from && (DateTime) i.DueDate <= (DateTime) to);
            if (totalCount == 0) return 1;

            return (double) successCount / totalCount;
        }

        //Số task đầy đủ thông tin / tổng số task
        public static double Kpi6(List<Issue> issues, DateTime? from, DateTime? to)
        {
            double count = 0;
            var list = issues.Where(i => i.CreatedOn != null &&
                (DateTime) i.CreatedOn >= (DateTime) from && (DateTime) i.CreatedOn <= (DateTime) to).ToList();
            foreach (var issue in list)
                if (issue.StartDate != null && issue.DueDate != null && issue.AssignedToId != null &&
                    issue.CustomValues.All(c => c.id == 1 || c.id == 2 || c.id == 3)) count++;
            if (issues.Count == 0) return 1;
            return count / issues.Count;
        }

        //Tỉ lệ số task được cập nhật bởi chức danh PA trong những việc không gán tên PA / tổng số task được cập nhật
        public static double Kpi7(List<Issue> issues, DateTime? from, DateTime? to)
        {
            double count = 0;
            foreach (var issue in issues)
                if (!Dao.UpdateByAssistance(issue.Id, from, to)) count++;
            if (issues.Count == 0) return 1;

            return count / issues.Count;
        }
    }
}