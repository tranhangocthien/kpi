﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Title = "Open excel file",
                DefaultExt = "xlsx",
                Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };

            if (dialog.ShowDialog() == DialogResult.OK)
                txtInput.Text = dialog.FileName;
        }

        private async void btnExport_Click(object sender, EventArgs e)
        {
            if (txtInput.Text == "")
            {
                MessageBox.Show("Please choose an input file!", "Error");
                return;
            }

            var dialog = new SaveFileDialog
            {
                Title = "Save file",
                DefaultExt = "xlsx",
                Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                btnExport.Enabled = false;
                btnPAKPI.Enabled = false;
                btnDevKPI.Enabled = false;
                var thread = CreateStatusThread();
                thread.Start();
                await Task.Run(() => { ExcelHandler.Instance().Kpi(txtInput.Text, dialog.FileName); });
                thread.Abort();
                lblStatus.Text = "";
                btnExport.Enabled = true;
                btnPAKPI.Enabled = true;
                btnDevKPI.Enabled = true;

                MessageBox.Show("Success");
            }
        }

        private async void btnPAKPI_Click(object sender, EventArgs e)
        {
            if (txtInput.Text == "")
            {
                MessageBox.Show("Please choose an input file!", "Error");
                return;
            }

            var dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
            {
                btnPAKPI.Enabled = false;
                btnExport.Enabled = false;
                btnDevKPI.Enabled = false;
                var thread = CreateStatusThread();
                thread.Start();
                await Task.Run(() => { ExcelHandler.Instance().PaKpi(txtInput.Text, dialog.SelectedPath); });
                thread.Abort();
                lblStatus.Text = "";
                btnPAKPI.Enabled = true;
                btnExport.Enabled = true;
                btnDevKPI.Enabled = true;

                MessageBox.Show("Success");
            }
        }

        private async void btnDevKPI_Click(object sender, EventArgs e)
        {
            if (txtInput.Text == "")
            {
                MessageBox.Show("Please choose an input file!", "Error");
                return;
            }

            var dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
            {
                btnPAKPI.Enabled = false;
                btnExport.Enabled = false;
                btnDevKPI.Enabled = false;
                var thread = CreateStatusThread();
                thread.Start();
                await Task.Run(() => { ExcelHandler.Instance().DevKpi(txtInput.Text, dialog.SelectedPath); });
                thread.Abort();
                lblStatus.Text = "";
                btnPAKPI.Enabled = true;
                btnExport.Enabled = true;
                btnDevKPI.Enabled = true;

                MessageBox.Show("Success");
            }
        }

        private Thread CreateStatusThread()
        {
            return new Thread(() =>
            {
                var i = 0;
                var text = "";
                while (true)
                {
                    text = i++ == 0 ? "Exporting." : text + ".";
                    i %= 5;
                    if (lblStatus.InvokeRequired)
                        lblStatus.Invoke(new MethodInvoker(delegate { lblStatus.Text = text; }));
                    else lblStatus.Text = text;
                    Thread.Sleep(300);
                }
            });
        }
    }
}