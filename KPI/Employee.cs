﻿using System.Collections.Generic;
using VecoLib.Model;

namespace KPI
{
    public class Employee
    {
        public Employee()
        {
            User = new User();
            Kpis = new List<Kpi>();
        }

        public User User { get; set; }
        public string Institue { get; set; }
        public List<Kpi> Kpis { get; set; }
    }
}